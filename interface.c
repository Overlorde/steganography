#include "interface.h"

GtkWidget *button_hide;
GtkWidget *button_unhide;

GtkWidget *button_ok_option_1;
GtkWidget *button_cancel_option_1;
GtkWidget *button_ok_option_2;
GtkWidget *button_cancel_option_2;
GtkWidget *path_to_image_2;
GtkWidget *path_to_text;
GtkWidget *path_to_image;
GtkWidget *content_user_message;
GtkWidget *dialog;
GtkWidget *checkbox;

GtkBuilder *builder;
GtkWidget *window;

void show_info(char *output) {

    GtkWidget *dialog;
    dialog = gtk_message_dialog_new(GTK_WINDOW(window),
                                    GTK_DIALOG_DESTROY_WITH_PARENT,
                                    GTK_MESSAGE_INFO,
                                    GTK_BUTTONS_OK,
                                    output);
    gtk_window_set_title(GTK_WINDOW(dialog), "Information");
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);
}

void on_window_main_destroy() {
    gtk_main_quit();
}

const gchar *get_text_of_textview(GtkWidget *text_view) {
    GtkTextIter start, end;
    GtkTextBuffer *buffer = gtk_text_view_get_buffer((GtkTextView *) text_view);
    gtk_text_buffer_get_bounds(buffer, &start, &end);
    return gtk_text_buffer_get_text(buffer, &start, &end, FALSE);
}

void on_input_text_changed_option_1() {
    if ((strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_text)), "") != 0 &&
         fopen(gtk_entry_get_text(GTK_ENTRY(path_to_text)), "r") != NULL &&
         strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "") != 0 &&
         fopen(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "r") != NULL &&
         strstr(gtk_entry_get_text(GTK_ENTRY(path_to_text)), ".txt") != NULL) ||
        (strcmp(get_text_of_textview(GTK_WIDGET(content_user_message)), "") != 0 &&
         strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "") != 0 &&
         fopen(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "r") != NULL &&
         strstr(gtk_entry_get_text(GTK_ENTRY(path_to_image)), ".txt") == NULL)) {
        gtk_widget_set_sensitive(button_ok_option_1, TRUE);
    } else {
        gtk_widget_set_sensitive(button_ok_option_1, FALSE);
    }
}

void on_input_text_changed_option_2() {
    if ((strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image_2)), "") != 0 &&
         fopen(gtk_entry_get_text(GTK_ENTRY(path_to_image_2)), "r") != NULL)) {
        gtk_widget_set_sensitive(button_ok_option_2, TRUE);
    } else {
        gtk_widget_set_sensitive(button_ok_option_2, FALSE);
    }
}

void on_checkbox_clicked() {
    if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(checkbox))) {
        GtkTextIter start, end;
        GtkTextBuffer *buffer = gtk_text_view_get_buffer((GtkTextView *) GTK_WIDGET(content_user_message));
        gtk_text_buffer_get_bounds(buffer, &start, &end);
        gtk_text_buffer_set_text(buffer, "", -1);

        gtk_widget_set_sensitive(content_user_message, FALSE);
        gtk_widget_set_sensitive(path_to_text, TRUE);
    } else {
        gtk_entry_set_text(GTK_ENTRY(path_to_text), "");
        gtk_widget_set_sensitive(path_to_text, FALSE);
        gtk_widget_set_sensitive(content_user_message, TRUE);
    }
}

void on_button_ok_1_clicked() {
    gchar *temp;
    if (strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "") != 0 &&
        strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_text)), "") != 0) {
        temp = (char *) gtk_entry_get_text(GTK_ENTRY(path_to_text));
    } else if (strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image)), "") != 0 &&
               strcmp(get_text_of_textview(GTK_WIDGET(content_user_message)), "") != 0) {
        temp = (char *) get_text_of_textview(
                GTK_WIDGET(content_user_message)); //check message wrong format -> must be pointer
    }
    gchar *argv[] = {"./main", "1", (char *) gtk_entry_get_text(GTK_ENTRY(path_to_image)), temp, NULL};
    char *output = NULL; // will contain command output
    GError *error = NULL;
    int exit_status = 0;
    if (!g_spawn_sync(NULL, argv, NULL, 0, NULL, NULL, &output, NULL, &exit_status, &error)) {
        printf(error->message);
    } else {
        printf("%s", output); //display the path of the new created image
        show_info("Your message has been successfully hidden in the image !");
    }
    g_free(output);
    g_free(error);
    gtk_entry_set_text(GTK_ENTRY(path_to_image), "");
    GtkTextIter start, end;
    GtkTextBuffer *buffer = gtk_text_view_get_buffer((GtkTextView *) GTK_WIDGET(content_user_message));
    gtk_text_buffer_get_bounds(buffer, &start, &end);
    gtk_text_buffer_set_text(buffer, "", -1);
    gtk_entry_set_text(GTK_ENTRY(path_to_text), "");
    gtk_widget_hide_on_delete(GTK_WIDGET(dialog));
}

void on_button_modal_clicked_cancel() {
    gtk_widget_hide(dialog);
}

void on_button_ok_2_clicked() {
    if (strcmp(gtk_entry_get_text(GTK_ENTRY(path_to_image_2)), "") != 0) {
        gchar *argv[] = {"./menu.sh", "2", (char *) gtk_entry_get_text(GTK_ENTRY(path_to_image_2)), NULL};
        char *output = NULL; // will contain command output
        GError *error = NULL;
        int exit_status = 0;
        if (!g_spawn_sync(NULL, argv, NULL, 0, NULL, NULL, &output, NULL, &exit_status, &error)) {
            printf(error->message);
        } else {
            printf("%s", output); //display the path of the new created image
            show_info(output);
        }
        g_free(output);
        g_free(error);
        gtk_entry_set_text(GTK_ENTRY(path_to_image_2), "");
        gtk_widget_hide_on_delete(GTK_WIDGET(dialog));
    }
}

void init_dialog_gui_option_1() {
    button_ok_option_1 = GTK_WIDGET(gtk_builder_get_object(builder, "button_ok"));
    button_cancel_option_1 = GTK_WIDGET(gtk_builder_get_object(builder, "button_cancel"));
    path_to_text = GTK_WIDGET(gtk_builder_get_object(builder, "path_to_text"));
    path_to_image = GTK_WIDGET(gtk_builder_get_object(builder, "path_to_image"));
    content_user_message = GTK_WIDGET(gtk_builder_get_object(builder, "content_user_message"));
    checkbox = GTK_WIDGET(gtk_builder_get_object(builder, "checkbox"));

    g_signal_connect(G_OBJECT(path_to_text), "changed", G_CALLBACK(on_input_text_changed_option_1), NULL);
    g_signal_connect(G_OBJECT(path_to_image), "changed", G_CALLBACK(on_input_text_changed_option_1), NULL);
    g_signal_connect(G_OBJECT(gtk_text_view_get_buffer(GTK_TEXT_VIEW(content_user_message))), "changed",
                     G_CALLBACK(on_input_text_changed_option_1), NULL);
    g_signal_connect(G_OBJECT(checkbox), "clicked", G_CALLBACK(on_checkbox_clicked), NULL);
    g_signal_connect(G_OBJECT(button_ok_option_1), "clicked", G_CALLBACK(on_button_ok_1_clicked), NULL);
    g_signal_connect(G_OBJECT(button_cancel_option_1), "clicked", G_CALLBACK(on_button_modal_clicked_cancel), NULL);
    g_signal_connect(G_OBJECT(dialog), "delete-event", G_CALLBACK(gtk_widget_hide_on_delete), NULL);

    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(checkbox), TRUE);
    gtk_widget_set_sensitive(button_ok_option_1, FALSE);
}

void init_dialog_gui_option_2() {

    button_ok_option_2 = GTK_WIDGET(gtk_builder_get_object(builder, "button_ok_2"));
    button_cancel_option_2 = GTK_WIDGET(gtk_builder_get_object(builder, "button_cancel_2"));
    path_to_text = GTK_WIDGET(gtk_builder_get_object(builder, "path_to_text"));
    path_to_image_2 = GTK_WIDGET(gtk_builder_get_object(builder, "path_to_image_2"));

    g_signal_connect(G_OBJECT(path_to_image_2), "changed", G_CALLBACK(on_input_text_changed_option_2), NULL);
    g_signal_connect(G_OBJECT(button_ok_option_2), "clicked", G_CALLBACK(on_button_ok_2_clicked), NULL);
    g_signal_connect(G_OBJECT(button_cancel_option_2), "clicked", G_CALLBACK(on_button_modal_clicked_cancel), NULL);
    g_signal_connect(G_OBJECT(GTK_WIDGET(dialog)), "delete-event", G_CALLBACK(gtk_widget_hide_on_delete), NULL);

    gtk_widget_set_sensitive(button_ok_option_2, FALSE);
}

// called when button is clicked
void on_button_hide_clicked() {
    dialog = GTK_WIDGET(gtk_builder_get_object(builder, "gtk_dialog_hide"));
    init_dialog_gui_option_1();
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(window));
    gtk_dialog_run(GTK_DIALOG(dialog));
}

void on_button_unhide_clicked() {
    dialog = GTK_WIDGET(gtk_builder_get_object(builder, "gtk_dialog_unhide"));
    init_dialog_gui_option_2();
    gtk_window_set_transient_for(GTK_WINDOW(dialog), GTK_WINDOW(window));
    gtk_dialog_run(GTK_DIALOG(dialog));
}

int main(int argc, char *argv[]) {

    gtk_init(&argc, &argv);

    builder = gtk_builder_new();
    gtk_builder_add_from_file(builder, "glade/interface.glade", NULL);

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));
    gtk_builder_connect_signals(builder, NULL);
    gtk_window_set_default_size(GTK_WINDOW(window), 450, 260);
    gtk_window_set_title(GTK_WINDOW(window), "Steganography Software");
    gtk_window_set_position(GTK_WINDOW(window), GTK_WIN_POS_CENTER);
    button_hide = GTK_WIDGET(gtk_builder_get_object(builder, "button_hide"));
    button_unhide = GTK_WIDGET(gtk_builder_get_object(builder, "button_unhide"));

    g_signal_connect(G_OBJECT(window), "destroy", G_CALLBACK(on_window_main_destroy), NULL);
    g_signal_connect(G_OBJECT(button_hide), "clicked", G_CALLBACK(on_button_hide_clicked), NULL);
    g_signal_connect(G_OBJECT(button_unhide), "clicked", G_CALLBACK(on_button_unhide_clicked), NULL);

    gtk_widget_show(window);
    gtk_main();

    return EXIT_SUCCESS;
}
