all: clean recup_msg dechiffre main interface

recup_msg: stego.c recup_msg.c
	$(CC) -Wall -g -lSDL2 -o recup_msg stego.c recup_msg.c

dechiffre: stego.c dechiffre.c
	$(CC) -Wall -g -o dechiffre stego.c dechiffre.c

main: steganographie_main.c
	$(CC) -Wall -g -lSDL2 -o main steganographie_main.c

interface: interface.c
	$(CC) -Wall -g `pkg-config gtk+-3.0 --cflags --libs` interface.c -o interface

clean:
	rm -f recup_msg dechiffre main interface image_convertie.ppm out.ppm #only clean after when make is done

$(info    Cleaning and compiling in progress...)
