# Steganography

Steganography is a program aimed to hide (option 1) / unhide (option 2) a message in any picture (will be converted to .ppm format).

## Installation

The easiest option to use the program is with docker installed.   
You can also execute the program on any linux with the same installation indicated in the DockerFile.

**With Docker :**
```bash
sudo docker build -t steganography:1.0 .
docker run --rm --interactive --env=DISPLAY=$DISPLAY --env=QT_X11_NO_MITSHM=1 --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw --name='stego' steganography:1.0
```
Run this command in terminal to allow X11 server host (GUI permission to display):
```bash
xhost +
```
## Usage

***A given example as demo is available to try :*** ```pigeon.png``` ```example.txt```

In order to get the image with the message hidden inside (option 1), run the following command to export the image to the repository of your choice:
```bash
docker cp stego:/src/out.ppm /your_repository
```

In order to find the message hidden inside the image (option 2), run the following command to import the image to the local /src repository of the docker image:
```bash
docker cp /your_repository/my_image.ppm stego:/src/my_image.ppm 
```

### Menu

![Alt text](menu.png?raw=true "Menu")

### Hide a message

![Alt text](option_1.png?raw=true "Hide a message")

### Display the image with a hidden message

![Alt text](example.png?raw=true "Example")

The image contains the following message (example.txt) :

> The messenger pigeon is a variety of domestic pigeon Columba livia domestica derived from the rock pigeon, selectively bred for its ability to find its way home over extremely long distances. The wild rock pigeon has an innate homing ability, meaning that it will generally return to its nest, it is believed using magnetoreception. This made it relatively easy to breed from the birds that repeatedly found their way home over long distances. Flights as long as 1,800 km 1,100 miles have been recorded by birds in competitive pigeon racing. Their average flying speed over moderate 965 km 600 miles distances is around 97 km/h 60 miles per hour and speeds of up to 160 km/h 100 miles per hour have been observed in top racers for short clarification needed distances. Because of this skill, messenger pigeons were used to carry messages as messenger pigeons. They are usually referred to as "pigeon post" if used in post service, or "war pigeon" during wars. Messenger pigeons are often incorrectly categorized as English Carrier pigeons, a breed of fancy pigeons selectively-bred for its distinctively rounded hard wattle. The purpose of using them was to send messages or mails.

**Impressive, isn't it ? :)**

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

This project is under MIT License.
