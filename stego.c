#include <errno.h>
#include <string.h>
#include "steganographie.h"

#define MAGIC_NUMBER 255

//Arret du programme
void erreur(const char *reason) {
    fprintf(stderr, "ERROR: %s\n", reason);
    exit(EXIT_FAILURE);
}

//Check si image de type P6, pour pouvoir lire le header correctement
int check_type(FILE *fp) {
    char temp = (char) fgetc(fp);
    if (temp == 'P' && (char) fgetc(fp) == '6') {
        fgetc(fp);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void skip_comments(FILE *fp) {
    char temp;
    while ((temp = (char) fgetc(fp)) ==
           '#') //Les lignes commençant par # ne doivent pas être prises en compte dans le Header.
    {
        while ((char) fgetc(fp) != '\n') {
        }
    }

    ungetc(temp, fp);
}

//Taille image
void get_image_dimensions(FILE *fp, unsigned int *dimensions) {
    errno = 0;
    char *temp = (char *) malloc(5 * sizeof(char));
    char *x = malloc(sizeof(char));
    fgets(temp, 5, fp); //start at P6
    if (!strcmp(temp, "P6")) {
        printf("Format is not P6. Exit");
        exit(EXIT_FAILURE);
    }
    free(temp);
    skip_comments(fp); //skip if comments
    fscanf(fp, "%s", x);
    dimensions[0] = strtoul(x, NULL, 10);
    if (errno == ERANGE || errno == EINVAL) {
        printf("An error appended finding dimension info.");
        exit(EXIT_FAILURE);
    }
    free(x);
    x = malloc(sizeof(char));
    fscanf(fp, "%s", x); //note: if image is malformed => reason is not the right dimension of the image
    dimensions[1] = strtoul(x, NULL, 10);
    if (errno == ERANGE || errno == EINVAL) {
        printf("An error appended finding dimension info.");
        exit(EXIT_FAILURE);
    }
    free(x);
}

int check_prof_img(FILE *fp) //Profondeur image, 1 si 255 & 0 sinon
{
    unsigned int c;
    char *temp = (char *) malloc(5 * sizeof(char));
    fgets(temp, 5, fp); //start at P6
    free(temp);
    temp = (char *) malloc(sizeof(char));
    fscanf(fp, "%s", temp);
    c = strtoul(temp, NULL, 10);
    if (errno == ERANGE || errno == EINVAL) {
        printf("An error appended finding depth color info.");
        exit(EXIT_FAILURE);
    }
    free(temp);
    if (c == MAGIC_NUMBER) //max depth color
    {
        fgetc(fp);
        return EXIT_SUCCESS;
    }
    return EXIT_FAILURE;
}