#ifdef _WIN32
#include <io.h>
#else
#define _POSIX_C_SOURCE 200809L

#include <sys/wait.h>
#include <unistd.h>

#endif

#include "steganographie.h"

#define T_MAX 100000
#define MAGIC_NUMBER 8

int main(__attribute__((unused)) int argc,
         char **argv) //2 modes de fonctionnement, un rapide, sans interface, & un plus expliqué.
{
    FILE *fp = NULL;

    //start here with interface
    fp = fopen(argv[1], "r+e");
    if (fp == NULL) //Erreur pendant l'ouverture
    {
        printf("Not able to open the file %s.\nFail...\n", argv[2]);
        return EXIT_FAILURE;
    }
    if (!check_type(fp)) {
        printf("\nProblem, file format is wrong : %s, trying to convert it to ppm format...\n", argv[1]);
        pid_t child_pid;
        int return_status = 0;
        child_pid = fork();
        if (child_pid == 0) {
            if (execlp("./script.sh", "script.sh", argv[1], NULL)) { /* execute the command  */
                printf("*** ERROR: exec failed\n");
                exit(EXIT_FAILURE);
            }
        }
        while (wait(&return_status) > 0) {
        }
    }
    fclose(fp);
    if (((fp = fopen("image_convertie.ppm", "r+e")) == NULL)) {
        printf("Not able to open the file %s.\nFail...\n", argv[1]);
        return EXIT_FAILURE;
    }
    char *msg = store_message(argv[2]);
    unsigned int message_taille = recup_taille_msg(msg);
    unsigned int *dimensions = (unsigned int *) malloc(2 * sizeof(unsigned int));
    get_image_dimensions(fp, dimensions);
    if (message_fits((int) message_taille, dimensions)) {
        if (check_prof_img(fp) == EXIT_SUCCESS) {
            FILE *fp_t = fopen("out.ppm", "we");
            if (fp_t == NULL) {
                printf("Not able to create the file out.ppm. Fail...");
                exit(EXIT_FAILURE);
            }
            unsigned int i = count_nb_lines(fp);
            recup_header(fp, i, fp_t);
            encode_taille(fp, fp_t, (message_taille - MAGIC_NUMBER) / MAGIC_NUMBER);
            encode_message(fp, fp_t, (message_taille - MAGIC_NUMBER), msg, dimensions);
            printf("Process terminated. Launching images...\n");
            fclose(fp);
            fclose(fp_t);
            free(msg);
            free(dimensions);
            pid_t child_pid;
            int return_status = 0;
            child_pid = fork();
            if (child_pid == 0) {
                if (execlp("eog", "eog", "out.ppm", NULL)) {
                    printf("*** ERROR: exec failed\n");
                    exit(EXIT_FAILURE);
                }
            }
            while (wait(&return_status) > 0) {
            }
        } else {
            printf("\nError: Image depth must be 255.\n");
            free(msg);
            free(dimensions);
            return EXIT_FAILURE;
        }
    } else {
        printf("\nError, dimensions of the image are not enought to fit the message.\nFail...\n");
        free(msg);
        free(dimensions);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}

void recup_header(FILE *fp1, unsigned int nb_lignes, FILE *fp2) {
    char temp;

    rewind(fp1); //Retourne au début du fichier

    for (unsigned int i = 0; i < nb_lignes; i++) {
        while ((temp = (char) fgetc(fp1)) != EOF && temp != '\n') {
            fputc(temp, fp2);
        }
        fputc('\n', fp2);
    }
}

unsigned int recup_taille_msg(const char mon_msg[]) //Recup taille message & calcule
{
    unsigned int i = 0;
    while (mon_msg[i] != '\0') {
        i++;
    }
    return i * MAGIC_NUMBER + MAGIC_NUMBER;
}

int message_fits(int taille, const unsigned int *dimensions) {
    return taille < dimensions[0] * dimensions[1] * 3;
}

unsigned int count_nb_lines(FILE *fp) {
    char temp;
    unsigned int count = 0;

    rewind(fp); //Retour en haut du fichier

    while ((temp = (char) fgetc(fp)) != EOF) {
        if (temp == '\n') {
            count++;
        }
    }
    return count;
}

void encode_taille(FILE *in, FILE *out, unsigned int taille) {
    char temp;
    unsigned int l;
    for (unsigned int i = 0; i < 10 * MAGIC_NUMBER; i++) //i varie de 1 a 8 sur les bits
    {
        temp = (char) fgetc(in);
        l = taille;      //on réinitialise l à chaque itération
        l >>= 79 - i; //décalage à droite pour bits de poids faible.
        if ((l & 1u) == 1) {
            if (((unsigned char) temp & 1u) == 0) {
                temp++; //si le bit vaut 0 dans le fichier, on le remplace par 1
            }
        } else {
            if (((unsigned char) temp & 1u) == 1) //inversement
            {
                temp--;
            }
        }
        fputc(temp, out); //On écrit ça dans le nouveau fichier.
    }
}

void
encode_message(FILE *in, FILE *out, unsigned int num_a_encod, const char *mon_msg, const unsigned int *dimensions) {
    int encod = 0;
    unsigned char temp;
    unsigned int idx = 0;
    unsigned int num = 0;
    unsigned char actuel;
    unsigned int fileSize = (dimensions[0] * dimensions[1] * 3) - 16; //Nombre de bits après le premier 8

    for (unsigned int i = 0; i < fileSize; i++) {
        temp = fgetc(in);
        actuel = mon_msg[idx]; //On prend les caractères 1 par 1.
        actuel >>= 7 - num;
        num++;
        if (encod <= num_a_encod) //num_a_encod = message_taille - 8 (premier octet=taille)
        {
            encod++;
            if ((actuel & 1u) == 1) {
                if ((temp & 1u) == 0) {
                    temp++; //comme dans la fonction, si le bit vaut 0 on l'incrémente à 1
                }
            } else {
                if ((temp & 1u) == 1) {
                    temp--; //inversement
                }
            }
            if (num == MAGIC_NUMBER) //Si num=8, on change de caractère car cela signifie qu'on a codé tous les bits.
            {
                idx++;
                num = 0;
            }
        }

        fputc(temp, out);
    }
}

char *store_message(char *input) {
    char *msg = (char *) malloc(T_MAX * sizeof(char));
    FILE *fichier = fopen(input, "r+e");
    if (fichier != NULL) //file
    {
        fgets(msg, T_MAX, fichier); // On lit maximum T_MAX caractères du fichier, on stocke le tout dans "chaine"
        fclose(fichier);
        return msg;
    }
    free(msg);
    return strdup(input); //plain text (be careful with spaces because bash args can be misinterpreted)
}
