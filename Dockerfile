FROM amd64/debian:10
WORKDIR /src
COPY . .
RUN apt-get update && \
    apt-get install --no-install-recommends -y make \
    gcc \
    libsdl2-dev \
    eog \
    imagemagick \
    libgtk-3-dev \
    dbus-x11 \
    libcanberra-gtk-module libcanberra-gtk3-module && \
    chmod +x ./menu.sh && chmod +x ./script.sh && \
    rm -rf /var/lib/apt/lists/* && make && chmod +x ./recup_msg && chmod +x ./dechiffre && chmod +x ./main 
CMD ./interface