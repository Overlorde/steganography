#ifdef _WIN32
#include <SDL.h>
#else

#include <SDL2/SDL.h>

#endif

int main(int argc, char *argv[]) {
    if (argc != 4) {
        printf("number of arguments must be equal to four.");
        return EXIT_FAILURE;
    }
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stdout, "Fail to initialize SDL (%s)\n", SDL_GetError());
        return EXIT_FAILURE;
    }
    char *cmd = (char *) malloc((strlen(argv[3]) + 100) * sizeof(char));
    strcpy(cmd, "./menu.sh");
    for (int i = 1; i <= 3; i++) {
        strcat(cmd, " ");
        strcat(cmd, argv[i]);
    }
    system(cmd);
    free(cmd);
    SDL_Quit();

    return EXIT_SUCCESS;
}
//COMPILER gcc -Wall -lSDL2-2.0 main.c -o main
