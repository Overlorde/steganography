#include "steganographie.h"

int main(__attribute__((unused)) int argc, char **argv) {
    FILE *fp;

    if ((fp = fopen(argv[1], "rbe")) == NULL) {
        printf("\nError, please enter a file to check.\n\n");
        return EXIT_FAILURE;
    }

    if (check_type(fp)) {
        char *temp = (char *) malloc(100 * sizeof(char));
        unsigned int count = 0;
        do {
            fgets(temp, 100, fp);
            count++;
        } while (strncmp(temp, "255", 3) != 0 && count < 10);
        free(temp);
        unsigned int taille = recup_taille_msg2(fp);
        printf("\nDecrypted successfully, this is what we have found: \n\n");
        decode_message2(taille, fp);
        fclose(fp);
    } else {
        printf("Error: File format is not .ppm.");
        return EXIT_FAILURE;
    }
    return 0;
}

//Trouve la taille du message
unsigned int recup_taille_msg2(FILE *fp) {
    char temp;
    unsigned int taille = 0;
    for (unsigned int i = 0; i < 10 * 8; i++) {
        temp = (char) fgetc(fp);
        if (i > 0) {
            taille <<= 1u; //décalage
        }

        taille |= ((unsigned char) temp & 1u); //Opération OU=
    }
    return taille;
}

void decode_message2(unsigned int taille, FILE *fp) {
    unsigned int limite = taille * 8;
    unsigned int num_actuel = 0;
    unsigned char charBuffer = 0;
    unsigned int temp;
    char *msg_secret = (char *) malloc(((unsigned long) taille + 1) * sizeof(char));
    unsigned int idx = 0;

    while (num_actuel <= limite) {
        temp = (unsigned char) fgetc(fp);
        if (msg_secret != NULL) {
            if (num_actuel % 8 == 0) {
                msg_secret[idx] = (char) charBuffer;
                idx++;
                charBuffer = 0;
            } else {
                charBuffer <<= 1u;
            }
        } else {
            erreur("Fail to initiate pointer msg_secret. Exit.");
        }
        charBuffer |= ((char) temp & 1u);
        num_actuel++;
    } //end while

    //On commence à 1 parce que le premier caractère ne fait pas partie du message.
    for (unsigned int i = 1; i < idx; i++) {
        printf("%c", msg_secret[i]);
    }
    printf("\n\n");
    free(msg_secret);
}
