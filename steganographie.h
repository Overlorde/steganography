#ifndef STENO
#define STENO

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
#include <io.h>
#include <SDL.h>
#else

#include <sys/wait.h>
#include <SDL2/SDL.h>

#endif

//prototype de fonction

unsigned int count_nb_lines(FILE *);

int check_type(FILE *);

void skip_comments(FILE *);

void get_image_dimensions(FILE *, unsigned int *);

int check_prof_img(FILE *);

//End program
void erreur(const char *reason);

//Check if P6 format
void recup_header(FILE *, unsigned int, FILE *);

unsigned int recup_taille_msg(const char[]);

unsigned int recup_taille_msg2(FILE *);

void decode_message2(unsigned int, FILE *);

int message_fits(int, const unsigned int *);

char *store_message(char *);

void encode_taille(FILE *, FILE *, unsigned int);

void encode_message(FILE *, FILE *, unsigned int, const char *, const unsigned int *);

#endif
