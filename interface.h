#ifndef STEGANOGRAPHY_INTERFACE_H
#define STEGANOGRAPHY_INTERFACE_H

#include <gtk/gtk.h>

#include <stdlib.h>
#include <stdio.h>

#endif //STEGANOGRAPHY_INTERFACE_H

void show_info(char *output);

void on_window_main_destroy();

const gchar *get_text_of_textview(GtkWidget *text_view);

void on_input_text_changed_option_1();

void on_input_text_changed_option_2();

void on_checkbox_clicked();

void on_button_ok_1_clicked();

void on_button_modal_clicked_cancel();

void on_button_ok_2_clicked();

void init_dialog_gui_option_1();

void init_dialog_gui_option_2();

void on_button_hide_clicked();

void on_button_unhide_clicked();

int main(int argc, char *argv[]);